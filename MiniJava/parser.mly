%{
  open LMJ
%}

%token <int32> INT_CONST
%token <bool> BOOL_CONST
%token INTEGER BOOLEAN
%token <string Location.t> IDENT
%token CLASS PUBLIC STATIC VOID MAIN STRING EXTENDS RETURN
%token PLUS MINUS TIMES NOT LT AND
%token COMMA SEMICOLON
%token ASSIGN
%token LPAREN RPAREN LBRACKET RBRACKET LBRACE RBRACE
%token THIS NEW DOT LENGTH
%token SYSO
%token IF ELSE WHILE
%token EOF

%left AND
%nonassoc LT
%left PLUS
%left MINUS
%left TIMES
%right NOT
%nonassoc LBRACKET
%nonassoc DOT



%start program

%type <LMJ.program> program

%%

program:
| m = mainclass d = list(clas) EOF
    {
      let c,a,i = m in 
        {
            name = c;
            defs = d;
            main_args = a;
            main = i
        }  
    }

mainclass:
| CLASS id = IDENT LBRACE
 PUBLIC STATIC VOID MAIN LPAREN STRING LBRACKET RBRACKET args = IDENT RPAREN LBRACE
 instr = instruction RBRACE RBRACE { (id, args, instr) }

clas:
| CLASS ic = IDENT ex = option_extends LBRACE decls = list(var_decl) ms = list(metho) RBRACE {(ic, {
          extends = ex;
          attributes = decls;
          methods = ms;
        })}
option_extends:
| { None }
| EXTENDS i = IDENT { Some(i) }
metho:
| PUBLIC mt = typ im = IDENT LPAREN lparams = separated_list(COMMA, param) RPAREN LBRACE d_i = decls_and_instrs RETURN r = expression SEMICOLON RBRACE {let decls, li = d_i in (im, {
            result = mt;
            formals = lparams;
            locals = decls;
            body = IBlock(li);
            return = r
        })}
decls_and_instrs:
| a = var_decl dr = decls_and_instrs {let drr, irr = dr in (a :: drr, irr)}
| i = instrs {([], i)}
instrs:
| i = instruction irs = instrs {i :: irs}
| {[]}
var_decl:
| t = typ i = IDENT SEMICOLON {(i, t)}
param:
| t = typ i = IDENT {(i, t)}

instruction:
| SYSO LPAREN e = expression RPAREN SEMICOLON { ISyso e }
| LBRACE il = nonempty_list(instruction) RBRACE { IBlock(il) }
| IF LPAREN e= expression RPAREN i = instruction ELSE i2 = instruction { IIf(e,i,i2) }
| WHILE LPAREN e = expression RPAREN i = instruction { IWhile(e,i) }
| i = IDENT ASSIGN e = expression SEMICOLON { ISetVar(i, e) }
| i = IDENT LBRACKET a = expression RBRACKET ASSIGN e = expression SEMICOLON { IArraySet(i, a, e) }

expression:
| e = raw_expression { Location.make $startpos $endpos e }
| LPAREN e = expression RPAREN { e }

raw_expression:
| i = IDENT { EGetVar i }
| THIS { EThis }
| c = constant { EConst c }
| u = unop; e = expression { EUnOp(u, e) }
| ea = expression; op = binop; eb = expression { EBinOp(op, ea, eb) }
| m = expression DOT id = IDENT LPAREN l = separated_list(COMMA, expression) RPAREN { EMethodCall(m, id, l) }
| a = expression LBRACKET i = expression RBRACKET {EArrayGet(a, i)}
| e = expression DOT LENGTH { EArrayLength(e) }
| NEW INTEGER LBRACKET idx = expression RBRACKET {  EArrayAlloc(idx) }
| NEW id = IDENT LPAREN RPAREN { EObjectAlloc(id) }

constant:
| b = BOOL_CONST { ConstBool b }
| i = INT_CONST { ConstInt i }





typ:
| INTEGER { TypInt }
| BOOLEAN { TypBool }
| INTEGER LBRACKET RBRACKET { TypIntArray }
| i = IDENT { Typ(i) }

%inline unop:
| NOT { UOpNot }
%inline binop:
| PLUS  { OpAdd }
| MINUS { OpSub }
| TIMES { OpMul }
| LT    { OpLt }
| AND   { OpAnd }





