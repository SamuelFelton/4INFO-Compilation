open MJ
open Map
open List
let header_decls: string = "
#include <stdio.h>
#include <stdlib.h>
#pragma GCC diagnostic ignored \"-Wpointer-to-int-cast\"
#pragma GCC diagnostic ignored \"-Wint-to-pointer-cast\"
struct array { int* array; int length; };\n"



let custom_type_decl n s = "struct " ^ n ^ "* " ^ s
let return_type_decl t = match t with
  | TypInt | TypBool -> "int"
  | TypIntArray -> "struct array*"
  | Typ(c) -> custom_type_decl c ""
let typ_str t =
  match t with
  | TypInt | TypBool -> "int"
  | TypIntArray -> "struct array*"
  | Typ(c) -> "struct " ^ c ^ "*"                   
let var_decl t s =
  typ_str t ^ " " ^ s
           
let if_template = format_of_string
                    "%sif(%s) {
%s
%s}
%selse {
%s
%s}
"
let while_template = format_of_string
                       "%swhile(%s) {
%s
%s}
"                     
type metho_c = { mc_name: string; owner_name: string; vtable_index: int; m: metho; }
type attr_c = { c_name: string; t: typ; }
type java2c_class = {
    name: string;
    attributes: attr_c StringMap.t;
    methods: metho_c StringMap.t;
  }
let attr_c_name cls_name attr_name = cls_name ^ "_" ^ attr_name
let metho_c_name cls_name metho_name = cls_name ^ "_" ^ metho_name
let vtable_name class_name = class_name ^ "_vtable"

                                                          
let id_from_ctx id caller m = if StringMap.mem id m.locals || List.exists (fun (n, _) -> n = id) m.formals then id else Printf.sprintf "this->%s" (StringMap.find id caller.attributes).c_name
let typ_of_id caller m id =
  if StringMap.mem id m.locals then StringMap.find id m.locals
  else
    try
      let (_, t)  = List.find (fun (n, _) -> n = id) m.formals in t
    with
    | Not_found -> (StringMap.find id caller.attributes).t
    | _ -> raise (Failure "couldn't find typ of id")
let rec typ_of_expr caller m c_classes_map = function
    EConst(_) | EBinOp(_,_,_) | EUnOp(_) | EArrayLength(_) | EArrayGet(_,_) -> TypInt
    | EThis -> Typ caller.name
    | EObjectAlloc(i) -> Typ i
    | EGetVar(id) -> typ_of_id caller m id
    | EArrayAlloc(_) -> TypIntArray
    | EMethodCall(e, i, _) ->
       let left_part_typ = typ_of_expr caller m c_classes_map e in
       match left_part_typ with
       | Typ(t) ->
          let le_caller = StringMap.find t c_classes_map in
          (StringMap.find i le_caller.methods).m.result
       | _ -> raise (Failure "Not sure this should happen")
let binop_to_str = function
    OpAdd -> "+"
  | OpSub -> "-"
  | OpMul -> "*"
  | OpLt -> "<"
  | OpAnd -> "&&"
let unop_to_str = function
    UOpNot -> "!"
let rec expr_to_str caller m c_class_map expr =
  let expr_of_ctx e = expr_to_str caller m c_class_map e in
  match expr with
  | EConst(c) ->
     begin
       match c with
       | ConstBool(b) -> if b then "1" else "0"
       | ConstInt(i) -> Int32.to_string i
     end
  | EGetVar(id) -> id_from_ctx id caller m
  | EThis -> "this"
  | EUnOp(u, e) ->
     Printf.sprintf "%s(%s)" (unop_to_str u) (expr_of_ctx e)
  | EBinOp(b, e1, e2) ->
     Printf.sprintf "%s %s %s" (expr_of_ctx e1)  (binop_to_str b)  (expr_of_ctx e2)
  | EArrayAlloc(e) ->
     let size_str = expr_of_ctx e in
     Printf.sprintf "({int sz_tmp = %s; struct array* tmp = (struct array*) calloc(sizeof(struct array), 1); tmp->length = sz_tmp; tmp->array = (int*) calloc(sizeof(int), sz_tmp); tmp;})" size_str
  | EObjectAlloc(id) ->
     let v_name = vtable_name id in
     Printf.sprintf "({struct %s* tmp = (struct %s*) calloc(sizeof(struct %s), 1); tmp->vtable = %s; tmp;})" id id id v_name
  | EMethodCall(el, i, ess) ->
     let el_t = typ_of_expr caller m c_class_map el in
     begin
     match el_t with
     
     | Typ(t) ->
        let corresponding_class = StringMap.find t c_class_map in
        let corresponding_method = StringMap.find i corresponding_class.methods in
        let vtable_idx = corresponding_method.vtable_index in
        let rt_typ_str = typ_str corresponding_method.m.result in
        let el_str = expr_of_ctx el in
        let left_part = Printf.sprintf "%s mtmp = %s;" (typ_str el_t) el_str in 
        let ess_str = String.concat "," (List.map expr_of_ctx ess) in
        let arg_call = if ess = [] then "(mtmp)" else Printf.sprintf "(mtmp, %s)" ess_str in
        Printf.sprintf "({%s(%s) mtmp->vtable[%d]%s;})" left_part rt_typ_str vtable_idx arg_call
     | _ -> raise (Failure "Not a custom type")
     end
  | EArrayLength(e) -> Printf.sprintf "%s->length" (expr_of_ctx e)
  | EArrayGet(el, ei) -> Printf.sprintf "%s->array[%s]" (expr_of_ctx el) (expr_of_ctx ei)

(*instruction*)
let rec instr_to_str tabs caller m c_class_map instr =
  let get_ntab_instr = instr_to_str (tabs ^ "\t") caller m c_class_map in
  let get_expr_ctx = expr_to_str caller m c_class_map in
  match instr with
  | IBlock(lst) -> String.concat "\n" (List.map (instr_to_str tabs caller m c_class_map) lst)
  | IIf(e, i1, i2) ->
     begin
       let i1_str = get_ntab_instr i1 in
       let i2_str = get_ntab_instr i2 in
       let expr_str = get_expr_ctx e in
       Printf.sprintf if_template tabs expr_str i1_str tabs tabs i2_str tabs
     end
  | IWhile(e, i) ->
     begin
       let expr_str = get_expr_ctx e in
       let instr_str = get_ntab_instr i in
       Printf.sprintf while_template tabs expr_str instr_str tabs
     end
  | ISetVar(id, e) ->
     begin
       let id_str = id_from_ctx id caller m in
       let rt = typ_of_id caller m id in
       Printf.sprintf "%s%s = (%s) (%s);" tabs id_str (typ_str rt) (get_expr_ctx e)
     end
  | IArraySet (id, et, eass) ->
     Printf.sprintf "%s%s->array[%s] = %s;" tabs (id_from_ctx id caller m) (get_expr_ctx et) (get_expr_ctx eass)
  | ISyso(expr) -> Printf.sprintf "%sprintf(\"%%d\\n\", %s);" tabs (get_expr_ctx expr)       

(*classes and methods*)
let get_parents cls defs_map =
  let rec aux_fun c aux = 
    match c.extends with
    | Some(p) -> let cp = StringMap.find p defs_map in
                 aux_fun cp ((cp, p)::aux)
    | None -> aux
  in aux_fun cls []
let attributes_map cls cls_name defs_map =
  let hierarchy = (cls, cls_name) :: get_parents cls defs_map in
  let fold_one_parent_class: attr_c StringMap.t -> MJ.clas * string -> attr_c StringMap.t = fun  mapAcc (c, c_name) ->
    let attr_list = StringMap.bindings c.attributes in
    List.fold_left (fun acc (n, t) -> StringMap.add n  { c_name = attr_c_name c_name n; t = t; } acc) mapAcc attr_list
  in
  List.fold_left fold_one_parent_class StringMap.empty hierarchy

let methods_map cls cls_name defs_map =
  let hierarchy = List.rev ((cls, cls_name) :: get_parents cls defs_map) in
  let rec insert_into_list caller_name n m l i = match l with
    | [] ->
       begin
         let record_i = { mc_name = metho_c_name caller_name n; vtable_index = i; m = m; owner_name = caller_name;  }
         in (n, record_i) :: []
       end
    | (v_n, v):: r ->
       if v_n = n then
         let record_i = { mc_name = metho_c_name caller_name n; vtable_index = i; m = m;  owner_name = caller_name; }
         in (n, record_i) :: r
       else
         (v_n, v) :: insert_into_list caller_name n m r (i + 1)
  in
  let fold_one: (string * metho_c) list -> MJ.clas * string -> (string * metho_c) list = fun acc_list (c, c_name) ->
    let m_list = StringMap.bindings c.methods in
    List.fold_left (fun inner_acc (mn, m) -> insert_into_list c_name mn m inner_acc 0) acc_list m_list
  in
  let table = List.fold_left fold_one [] hierarchy in
  StringMap.of_association_list table
                                
let convert_class cls cls_name defs_map =
  {
    name = cls_name;
    attributes = attributes_map cls cls_name defs_map;
    methods = methods_map cls cls_name defs_map
  }
let convert_classes defs_map =
  let class_list = StringMap.bindings defs_map in
  List.map (fun (name, c) -> convert_class c name defs_map) class_list




let class_decls(p : MJ.program): string = String.concat "\n" (List.map (fun (k,c) -> (Printf.sprintf "struct %s;" k)) (StringMap.to_association_list p.defs))

let method_decl owner metho_c =
  let this_param = custom_type_decl owner.name "this" in
  let other_params = String.concat ", " (List.map (fun (n, t) -> var_decl t n) metho_c.m.formals) in
  let all_params = if (String.length other_params) = 0 then this_param else this_param ^ ", " ^ other_params in
  "void* " ^ metho_c.mc_name ^ "(" ^ all_params ^ ")"
                                                      
let method_locals metho =
  let local_decls = List.map (fun (id, t) -> var_decl t id ^ ";") (StringMap.bindings metho.locals) in
  "\t" ^ String.concat "\n\t" local_decls


let method_body owner metho_c c_class_map =
  let header = method_decl owner metho_c in
  let locals = method_locals metho_c.m ^ "\n" in
  let instr_str = instr_to_str "\t" owner metho_c.m c_class_map metho_c.m.body in
  let body = locals ^ instr_str in
  let result = "\treturn (void*) " ^ (expr_to_str owner metho_c.m c_class_map metho_c.m.return) ^ ";" in  header ^ " {\n" ^ body ^ "\n" ^ result ^ "\n}"

let distinct_methods cs =
  let inner_fold mapAcc c =
    List.fold_left (fun inner_acc (m_name, m_c) ->
        let k = m_c.mc_name in
        begin
          if StringMap.mem k inner_acc then inner_acc else
            begin
              StringMap.add k m_c inner_acc
            end
        end) mapAcc (StringMap.bindings c.methods)
  in List.fold_left inner_fold StringMap.empty cs

let vtable_decl c =
  let start = "void* (*" ^ vtable_name c.name ^ "[])() = {" in
  let method_names = List.map (fun (m_name, m_c) -> m_c.mc_name) (StringMap.bindings c.methods) in
  let joined_names = String.concat ", " method_names in
  start ^ joined_names ^ "};"
let all_vtables cs =
  let vtables = List.map (fun c -> vtable_decl c) cs in
  String.concat "\n" vtables
                                     
let class_body c =
  let opening = Printf.sprintf "struct %s {\n" c.name in
  let vtable_decl = "\tvoid* (**vtable)();\n\t" in
  let all_attrs = StringMap.bindings c.attributes in
  let attributes = String.concat "\n\t" (List.map (fun (_, a) -> var_decl a.t a.c_name ^ ";")  all_attrs) in
  opening ^ vtable_decl ^ attributes ^ "\n};"

let main_body p c_class_map =
  let head = "int main(int argc, char** argv) {\n" in
  let main_class = { name = ""; attributes = StringMap.empty; methods = StringMap.empty } in
  let main_method = { formals = []; result = TypInt; locals = StringMap.empty; body = p.main; return = EConst (ConstInt Int32.zero);} in
  let return_str = "\treturn 0;" in
  let main_method_c = {mc_name = "main"; owner_name = p.name; vtable_index = 0; m = main_method} in
  Printf.sprintf "%s\n%s\n%s\n}" head (instr_to_str "\t" main_class main_method c_class_map p.main) return_str
                                            
let program2c (p : MJ.program) : unit =
  let java2c_classes = convert_classes p.defs in
  let class_de = (class_decls p) in
  let java2c_classes_map = List.fold_left (fun acc c -> StringMap.add c.name c acc) StringMap.empty java2c_classes in
  let distinct_meths = distinct_methods java2c_classes in
  let class_bodies = String.concat "\n" (List.map (fun c -> class_body c) java2c_classes) in
  let method_bodies = String.concat "\n" (List.map (fun (mn, m_c) -> method_body (StringMap.find m_c.owner_name java2c_classes_map) m_c java2c_classes_map) (StringMap.bindings distinct_meths)) in
  let method_declarations = String.concat "\n" (List.map (fun (mn, m_c) -> method_decl (StringMap.find m_c.owner_name java2c_classes_map) m_c ^ ";") (StringMap.bindings distinct_meths)) in
  let vtable_defs = all_vtables java2c_classes in
  let mb = main_body p java2c_classes_map in 
  let parts = header_decls :: class_de :: method_declarations :: vtable_defs :: class_bodies :: method_bodies :: mb :: [] in
  let full = String.concat "\n" parts in
  (Printf.printf "%s\n" full);

                                       
