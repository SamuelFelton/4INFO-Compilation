open LMJ

exception Error of string

let rec complete_class c defs =
  match c.extends with
  | None -> c
  | Some(p) ->
     let p_complete = complete_class (StringMap.find (Location.content p) defs) defs in
     let p_complete_methods_not_overriden = List.filter (fun (id, _) -> (List.exists (fun (id2, _) -> (Location.content id) = (Location.content id2)) c.methods) = false) p_complete.methods in
     {
       extends = Some(p);
       attributes = c.attributes @ p_complete.attributes;
       methods = c.methods @ p_complete_methods_not_overriden;
     }

let rec is_subtype child sought_parent_id defs =
  match child.extends with
  | None -> false
  | Some(sought_parent_id) -> true
  | Some(x) -> is_subtype (StringMap.find (Location.content x) defs) sought_parent_id defs

(* NOT COMMUTATIVE*)
let is_same_type c p class_defs =
  match (c, p) with
  | (Typ a, Typ b) -> if (Location.content a) = (Location.content b) || is_subtype (StringMap.find (Location.content b) class_defs) (Location.content a) class_defs then true
                      else false
  | (a, b) -> if a = b then true else false
                                        
let make_method_env complete_caller caller_typ m =
  let visible_caller_attrs = List.filter (fun (i, _) -> (List.exists (fun (i2, _) -> (Location.content i) = (Location.content i2)) m.locals) = false) complete_caller.attributes in
  let all_available_vars = m.formals @ m.locals @ visible_caller_attrs in
  List.fold_left (fun env (id, typ) -> StringMap.add (Location.content id) typ env) (StringMap.add "this" caller_typ StringMap.empty) all_available_vars

let error_of_loc l msg =
  raise (Error (Printf.sprintf "%s: %s" (Error.positions (Location.startpos l) (Location.endpos l)) msg))
let raise_if_not_empty msg_of_loc = function
    [] -> ()
  | (h,_)::t -> error_of_loc h (msg_of_loc h)
                  
let str_of_typ = function
    TypInt -> "int"
  | TypBool -> "boolean"
  | TypIntArray -> "int[]"
  | Typ(c) -> (Location.content c)
        
let rec typ_expr id_to_typ class_defs expr =
  let typ_expr_curr = typ_expr id_to_typ class_defs in
  match Location.content expr with
  | EConst(ConstBool _) -> TypBool
  | EConst(ConstInt _) -> TypInt
  | EUnOp(op, e) ->
     begin
       let typ_e = typ_expr_curr e in
       match op with
       | UOpNot -> if typ_e <> TypBool then error_of_loc e "! is applied to a non boolean" else TypBool
     end
  | EBinOp(b, e1, e2) ->
     begin
       let typ_e1 = typ_expr_curr e1 in
       let typ_e2 = typ_expr_curr e2 in
       if typ_e1 <> typ_e2 then error_of_loc e1 "Operands of binop are of a different type"
       else match b with
            | OpAnd -> if typ_e1 = TypBool then TypBool else error_of_loc e1 "&& applied to ints"
            | OpLt -> if typ_e1 = TypInt then TypBool else error_of_loc e1 "Comparison applied to booleans"
            | _ -> if typ_e1 = TypInt then TypInt else error_of_loc e1 "arithmetic operation applied to booleans"
     end
  | EThis -> StringMap.find "this" id_to_typ
  | EObjectAlloc(id) -> if StringMap.mem (Location.content id) class_defs then Typ(id)
                        else error_of_loc expr (Printf.sprintf "Unknown class: %s" (Location.content id)) 
  | EArrayLength(a) ->
     let typ_a = typ_expr_curr a in
     if typ_a = TypIntArray then TypInt else error_of_loc a "Calling array length on non array type"
  | EArrayGet(a, i) ->
     let typ_a = typ_expr_curr a in
     let typ_i = typ_expr_curr i in
     if typ_a <> TypIntArray then error_of_loc a "Trying to index something that is not an array"
     else if typ_i <> TypInt then error_of_loc i "Trying to index an array with non integer"
     else TypInt
  | EArrayAlloc(sze) ->
     let typ_sze = typ_expr_curr sze in
     if typ_sze <> TypInt then error_of_loc sze "Trying to allocate an array with non integer size"
     else TypIntArray
  | EGetVar(id) ->
     begin
       try StringMap.find (Location.content id) id_to_typ
       with
       | Not_found -> error_of_loc expr "Variable does not exist" 
       | _ -> raise (Failure "unexpected exception")
     end
  | EMethodCall(left_e, m_id, params) ->
     begin
       let left_e_typ = typ_expr_curr left_e in
       let params_with_typ = List.map (fun p -> (p, typ_expr_curr p)) params in
       match left_e_typ with
       | Typ(left_id) ->
          begin
            let left_class = StringMap.find (Location.content left_id) class_defs in
            try
              let (_, called_m) = List.find (fun (n,_) -> (Location.content n) = (Location.content m_id)) left_class.methods in
              let actual_list_size = List.length params in
              let expected_size = List.length called_m.formals in
              if actual_list_size <> expected_size then error_of_loc left_e "Wrong number of argument"
              else
                let expected_types = List.map (fun (_, t) -> t) called_m.formals in
                let params_zipped = List.combine params_with_typ expected_types in
                let not_matching_params = List.filter (fun ((_, t), et) -> is_same_type et t class_defs = false) params_zipped in
                match not_matching_params with
                | [] -> called_m.result
                | ((e, t), exp_t)::_ -> error_of_loc e (Printf.sprintf "Wrong argument type: expected %s, got %s" (str_of_typ exp_t) (str_of_typ t))
            with
            | Not_found -> error_of_loc left_e (Printf.sprintf "Method %s not found for type %s" (Location.content m_id) (str_of_typ left_e_typ))
          end
       | _ -> error_of_loc left_e "Trying to call method on non-object type"
     end

let rec typecheck_instr id_to_typ class_defs instr =
  let instr_curr = typecheck_instr id_to_typ class_defs in
  match instr with
  | IBlock (lst) -> List.iter instr_curr lst
  | IIf(e, i1, i2) ->
     let e_typ = typ_expr id_to_typ class_defs e in
     if e_typ <> TypBool then error_of_loc e "If condition must be boolean"
     else
       let () = instr_curr i1 in
       instr_curr i2
  | IWhile(e, i) ->
     let e_typ = typ_expr id_to_typ class_defs e in
     if e_typ <> TypBool then error_of_loc e "While condition must be boolean"
     else instr_curr i
  | ISyso(e) ->
     let e_typ = typ_expr id_to_typ class_defs e in
     if e_typ <> TypInt then error_of_loc e "System.out.println must output integer"
     else ()
  | ISetVar(id, e) ->
     begin
       try
         let left_typ = StringMap.find (Location.content id) id_to_typ in
         let () = Printf.printf "%s = %s\n" (Location.content id) (str_of_typ left_typ) in
         let right_typ = typ_expr id_to_typ class_defs e in
         if is_same_type left_typ right_typ class_defs then ()
         else error_of_loc e (Printf.sprintf "expected %s, got %s" (str_of_typ left_typ) (str_of_typ right_typ))
       with
       | Not_found -> error_of_loc id (Printf.sprintf "%s: variable not found" (Location.content id))
     end
  | IArraySet(id, idx_expr, val_expr) ->
     begin
       let array_typ = StringMap.find (Location.content id) id_to_typ in
       let idx_typ = typ_expr id_to_typ class_defs idx_expr in
       let val_typ = typ_expr id_to_typ class_defs val_expr in
       if array_typ <> TypIntArray then error_of_loc id "Trying to index a variable that is not an array"
       else if idx_typ <> TypInt then error_of_loc idx_expr "Array index must be integer"
       else if val_typ <> TypInt then error_of_loc val_expr "Assigned array value must be an integer"
       else ()
     end

let typecheck_method caller caller_typ m class_defs =
  let () = Printf.printf "typechecking method of %s\n" (str_of_typ caller_typ) in 
  let env = make_method_env caller caller_typ m in
  let params_also_in_class = List.filter (fun (id, _) -> List.exists (fun (cid, _) -> (Location.content id) = (Location.content cid)) caller.attributes) m.formals in
  let params_also_in_locals = List.filter (fun (id, _) -> List.exists (fun (cid, _) -> (Location.content id) = (Location.content cid)) m.locals) m.formals in
  let return_typ = typ_expr env class_defs m.return in
  let () = raise_if_not_empty (fun id -> Printf.sprintf "%s: this parameter is already present in locals" (Location.content id)) params_also_in_locals in
  let () = raise_if_not_empty (fun id -> Printf.sprintf "%s: this parameter is already present in class" (Location.content id)) params_also_in_class in
  let () = typecheck_instr env class_defs m.body in
  if is_same_type return_typ m.result class_defs then ()
  else error_of_loc m.return (Printf.sprintf "Wrong return type: expected %s, got %s" (str_of_typ m.result) (str_of_typ return_typ))
           
let typecheck_class caller caller_typ class_defs =
  List.iter (fun (_, m) -> typecheck_method caller caller_typ m class_defs) caller.methods
let check_multiple_class_defs p =
  let rec aux defs acc =
    match defs with
    | [] -> ()
    | (id, _)::t ->
       begin
         let name = Location.content id in
         if StringMap.mem name acc then error_of_loc id (Printf.sprintf "%s: class already defined" name)
         else aux t (StringMap.add name name acc)           
       end
  in let main_name = Location.content p.name in
  aux p.defs (StringMap.add main_name main_name StringMap.empty)
let distinct_methods p full_classes =
  List.fold_left (fun acc (owner_id, c) -> let owner = StringMap.find (Location.content owner_id) full_classes in
                                           let owner_t = Typ owner_id in
                                           List.fold_left (fun acc (mid, m) -> (owner, owner_t, mid, m) :: acc) acc c.methods) [] p.defs
               
let typecheck_program (p : program) : unit =
  let class_map = List.fold_left (fun m (id, c) -> StringMap.add (Location.content id) c m) StringMap.empty p.defs in
  let full_classes = StringMap.map (fun c -> complete_class c class_map) class_map in
  let methods_to_check = distinct_methods p full_classes in
  let () = List.iter (fun (id, c) -> List.iter (fun (mid, _) -> Printf.printf "\t%s\n" (Location.content mid)) c.methods) p.defs in
  let () = check_multiple_class_defs p in
  let () = typecheck_instr StringMap.empty full_classes p.main in
  List.iter (fun (o, ot, mid, m) -> typecheck_method o ot m full_classes) methods_to_check
  (*StringMap.iter (fun name c ->
      let (id , _) = List.find (fun (id, _) -> (Location.content id) = name) p.defs in
      typecheck_class c (Typ id) full_classes) full_classes*)
